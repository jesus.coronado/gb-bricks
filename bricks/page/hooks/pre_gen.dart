import 'package:mason/mason.dart';

void run(HookContext context) {
  final pageType = context.vars['page_type'].toString().toLowerCase();
  final isStateless = pageType == 'stateless';

  context.vars = {
    ...context.vars,
    'isStateless': isStateless,
  };
}
